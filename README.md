# Recipe App proxy

with NGINX!

## Usage

### Environment variables

- `LISTEN_PORT` - port to listen (default: 8000)
- `APP_HOST` - Hostname (default: 'app')
- `APP_PORT` - default: 9000
